import { Component, OnInit } from '@angular/core';
import { MealService } from '../meal.service';
import { Observable } from 'rxjs/Observable';
import { Meal } from '../core/meal'
import { DatePipe } from '@angular/common/';

@Component({
  selector: 'app-month-view',
  templateUrl: './month-view.component.html',
  styleUrls: ['./month-view.component.styl'],
  providers: [DatePipe]
})
export class MonthViewComponent implements OnInit {

  currentMonth: number = new Date().getMonth()+1;
  currentYear: number = new Date().getFullYear();
  currentDayCount: number

  addMode: boolean = false

  mealList: Meal[] = []
  meals: Observable<Meal[]>;
  columns: string[];

  constructor(private datePipe: DatePipe,private mealService: MealService) { }

  ngOnInit() {
    this.currentDayCount = this.daysInMonth()


    for (let i = 1; i <= this.currentDayCount; i++) {
      let newMeal = <Meal>{
        date: this.currentYear+"-"+this.currentMonth+"-"+i,
        name: ""
      }
      this.mealList.push(newMeal)
    }

    this.columns = this.mealService.getColumns();
    this.mealService.getMeals().subscribe((meals: Meal[]) => {
      meals.forEach(function(meal){
        let day = this.datePipe.transform(meal.date,'dd')
        console.log(meal)
        this.mealList[day-1].name = meal.name
        console.log(this.mealList)
      }, this)
    })
  }

  daysInMonth(): number {
    return new Date(this.currentYear,this.currentMonth,0).getDate();
  }

  addNewMode() {
    this.addMode = true
  }
}
