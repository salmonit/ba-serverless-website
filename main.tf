
resource "aws_s3_bucket" "app-frontend" {
  
  website {
    index_document = "index.html"
  }

  provisioner "local-exec" {
    command = "aws s3 cp app/dist/frontend s3://${self.id} --recursive"
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "aws s3 rm s3://${self.id} --recursive"
  }
}

resource "aws_s3_bucket_policy" "app-frontend-policy" {
    bucket = "${aws_s3_bucket.app-frontend.id}"

    policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[{
 	  "Sid":"PublicReadForGetBucketObjects",
    "Effect":"Allow",
 	  "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["${aws_s3_bucket.app-frontend.arn}/*"]
  }]
}
POLICY
}


output "webiste-name" {
  value = "${aws_s3_bucket.app-frontend.website_endpoint}"
}

